import 'package:flutter/material.dart';
import 'home_movie.dart';
import 'login_page.dart';
import 'home_page.dart';
import 'herdiana.dart';
import 'derry.dart';
import 'faisal.dart';
import 'radea.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
    Herdiana.tag: (context) => Herdiana(),
    Derry.tag: (context) => Derry(),
    Faisal.tag: (context) => Faisal(),
    Radea.tag: (context) => Radea(),
    HomeMovie.tag: (context) => HomeMovie(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kodeversitas',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
