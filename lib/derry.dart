import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Derry extends StatelessWidget {
  static String tag = 'derry';
  var derry = ['assets/derry.jpeg'];

  @override
  Widget build(BuildContext context) {
    final derry = Hero(
      tag: 'derry',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
            radius: 72.0,
            backgroundColor: Colors.transparent,
            backgroundImage: AssetImage('assets/derry.jpeg')),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Derry Efrillian Sephina',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Perkenalkan nama saya Derry Efrillian Sephina, kuliah di Sekolah Tinggi Teknologi Bandung dengan NIM 18111191. Saya lahir di Bandung pada tanggal 25 April tahun 2000, hobi saya menulis juga merangkai sajak, selain itu juga saya suka belajar atau memperdalam dunia coding atau IT. Alamat tinggal rumah saya di Komplek Cingcin Permata Indah blok H-167. Terimakasih!',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[derry, welcome, lorem],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
